# README #

Explanation of the used technologies and the changes that need to be made before going into a production environment.

### Used Technologies ###

* Spring Boot:
Spring technology that makes it easy to: create Maven/Gradle project and download needed dependencies, develop the app and deploy it in a server.
* Swagger:
A tool that allows to describe the structure of the APIs in an app and provide an UI to describe and test them.
* HSqldb:
SQL Database (already embedded in Spring Boot) used to store data in-memory.
* Tomcat:
Application server in where the app is deployed

### Going into a production environment ###

* Securize the APIs.
* Disable or securize swagger.
* Change the configuration of the database to adapt it to production needs and configure it to be permanent instead of in memory (if the storage in memory was only for testing purposes).
* The Tomcat server could be worth to contain the application in production but it would be advisable to change / increase the default configuration.
* Add Spring Actuator that provides several useful services for an application in production.
* Add a custom exceptions and errors system.

