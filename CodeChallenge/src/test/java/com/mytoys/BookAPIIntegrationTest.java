package com.mytoys;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.mytoys.book.repository.IBookRepository;
import com.mytoys.book.service.BookService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookAPIIntegrationTest {
	
	@LocalServerPort
    private int port;
	
	@Autowired
    private TestRestTemplate restTemplate;
	@Autowired
	private BookService bookService;
	@Autowired
	private IBookRepository bookRepository;
	
	@Test
	public void searchThroughGoogleBookAPISuccessfuly() throws Exception {
		ResponseEntity<String> responseEntity = restTemplate.getForEntity("/book/findByQuery/reverte", String.class);
		
		assertThat(responseEntity.getBody()).isEqualTo("Retrieved books stored successfully");
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(false, bookRepository.findAll().isEmpty());
		assertThat(bookService.getAllStoredBooks().contains("reverte"));
	}
	
	@Test
	public void searchThroughGoogleBookAPINotFound() throws Exception {
		ResponseEntity<String> responseEntity = restTemplate.getForEntity("/book/findByQuery/impossiblequery1234", String.class);

		assertThat(responseEntity.getBody()).isEqualTo("Retrieved books stored successfully");
		// No "Not Found" returned security wise
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(true, bookRepository.findAll().isEmpty());
	}
	
}
