package com.mytoys.book.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mytoys.book.service.BookService;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
    private BookService bookServiceMock;
	
	@Test
    public void getBooksByQuery_withQueryValid() throws Exception {
        mockMvc.perform(get("/book/findByQuery/reverte"))
                .andExpect(status().isOk())
        		.andExpect(content().string("Retrieved books stored successfully"));
 
        verify(bookServiceMock, times(1)).searchThroughGoogleBookAPI("reverte");
        verifyNoMoreInteractions(bookServiceMock);
    }
	
	@Test
    public void getBooksByQuery_withQueryNull() throws Exception {
        mockMvc.perform(get("/book/findByQuery/"))
                .andExpect(status().isNotFound());
 
        verify(bookServiceMock, times(0)).searchThroughGoogleBookAPI(null);
        verifyNoMoreInteractions(bookServiceMock);
    }
	
	@Test
    public void getBooksByQuery_withQueryEmpty() throws Exception {
        mockMvc.perform(get("/book/findByQuery/"))
                .andExpect(status().isNotFound());
 
        verify(bookServiceMock, times(0)).searchThroughGoogleBookAPI("");
        verifyNoMoreInteractions(bookServiceMock);
    }

}
