package com.mytoys.book.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Book {

	@Id
	@GeneratedValue
	private Long id;

	@Column(length = 20000)
	private String bookjson;

	private String googleBookId;
	private String title;
	private String authors;
	private String publisher;
	private String publishedDate;
	@Column(length = 10000)
	private String description;
	private Integer pageCount;

	public Book() {
	}

	public Book(String bookjson) {
		super();
		this.bookjson = bookjson;
	}

	public Book(String bookjson, String googleBookId, String title, String authors, String publisher,
			String publishedDate, String description, Integer pageCount) {
		super();
		this.bookjson = bookjson;
		this.googleBookId = googleBookId;
		this.title = title;
		this.authors = authors;
		this.publisher = publisher;
		this.publishedDate = publishedDate;
		this.description = description;
		this.pageCount = pageCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBookjson() {
		return bookjson;
	}

	public void setBookjson(String bookjson) {
		this.bookjson = bookjson;
	}

	public String getGoogleBookId() {
		return googleBookId;
	}

	public void setGoogleBookId(String googleBookId) {
		this.googleBookId = googleBookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", bookjson=" + bookjson + ", googleBookId=" + googleBookId + ", title=" + title
				+ ", authors=" + authors + ", publisher=" + publisher + ", publishedDate=" + publishedDate
				+ ", description=" + description + ", pageCount=" + pageCount + "]";
	}

}