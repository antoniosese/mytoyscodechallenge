package com.mytoys.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mytoys.book.service.IBookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/book")
@Api(value = "book", description = "Book API store in a in-memory database the list of books suggested by the search API provided by Google Books API.")
public class BookController {
	
	private static final String EMPTY_QUERY = "You need to specify a query (cannot be null or empty).";
	private static final String EXCEPTION_IN_SEARCH = "There was a problem during the search. Please, try again.";
	private static final String EXCEPTION_IN_GET_ALL_STORED = "There was a problem retrieving all the stored books. Please, try again.";
	private static final String STORED_SUCCESS = "Retrieved books stored successfully";

	@Autowired
	IBookService bookService;

	@RequestMapping(value = "/findByQuery/{query}", method = RequestMethod.GET)
	@ApiOperation(value = "Store all retrieved books suggested by Google Book API search filtered by query.", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
							@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
							@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
							@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	public String getBooksByQuery(@PathVariable String query) {
		
		String result = "";
		
		if( query == null || query.trim().isEmpty() ) {
			result = EMPTY_QUERY;
		} else {
			try {
				bookService.searchThroughGoogleBookAPI(query);
				result = STORED_SUCCESS;
			} catch (Exception e) {
				result = EXCEPTION_IN_SEARCH;
			}
		}
		
		return result;
	}
	
	@RequestMapping(value = "/findAllStoredBooks", method = RequestMethod.GET)
	@ApiOperation(value = "JUST FOR MANUAL TESTING PURPOSE. Retrieve all books stored in the in-memory database.", response = String.class)
	public String getAllStoredBooks() {
		
		String result = "";
		
		try {
			result = bookService.getAllStoredBooks();
		} catch (Exception e) {
			result = EXCEPTION_IN_GET_ALL_STORED;
		}
		
		return result;
	}

}

