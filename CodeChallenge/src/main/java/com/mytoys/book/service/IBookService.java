package com.mytoys.book.service;

public interface IBookService {
	
	public void searchThroughGoogleBookAPI(String query);
	
	public String getAllStoredBooks();

}
