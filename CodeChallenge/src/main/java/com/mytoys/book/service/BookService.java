package com.mytoys.book.service;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.JsonNode;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mytoys.book.entity.Book;
import com.mytoys.book.repository.IBookRepository;

@Service
public class BookService implements IBookService {
	
	private static final String GOOGLE_BOOK_API_SEARCH = "https://www.googleapis.com/books/v1/volumes?q=";
	
	private IBookRepository bookRepository;
	
	@Autowired
	public BookService(IBookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	public void searchThroughGoogleBookAPI(String query) {
		
		String searchURL = GOOGLE_BOOK_API_SEARCH + query;

		RestTemplate template = new RestTemplate();

		ResponseEntity<String> response = template.getForEntity(searchURL, String.class);
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;
		
		try {
			root = mapper.readTree(response.getBody());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// If the response doesn't have "items" there were no books suggested by
		// the Google Book API for the current query so there is nothing to
		// store. No "Not Found" returned security wise
		if (root.has("items")) {
			
			JsonNode name = root.path("items");
			Iterator<JsonNode> itr = name.iterator();
	        while (itr.hasNext()) {
	        	JsonNode node = itr.next();
	        	Book book = new Book(node.toString(),
	        			(node.has("id")) ? (node.get("id").textValue()) : "No id",
	        			(node.get("volumeInfo").has("title")) ? (node.get("volumeInfo").get("title").textValue()) : "No title",
	   					(node.get("volumeInfo").has("authors")) ? (node.get("volumeInfo").get("authors").textValue()) : "No authors",
	   					(node.get("volumeInfo").has("publisher")) ? (node.get("volumeInfo").get("publisher").textValue()) : "No publisher",
	        			(node.get("volumeInfo").has("publishedDate")) ? (node.get("volumeInfo").get("publishedDate").textValue()) : "No published date",
	        			(node.get("volumeInfo").has("description")) ? (node.get("volumeInfo").get("description").toString()) : "No description",
	        			(node.get("volumeInfo").has("pageCount")) ? (node.get("volumeInfo").get("pageCount").asInt()) : 0);
	        	bookRepository.save(book);
	        }
	        
		}
        
	}
	
	public String getAllStoredBooks() {
		
		StringBuilder sb = new StringBuilder();
		List<Book> books = bookRepository.findAll();
		Iterator<Book> it = books.iterator();
		while (it.hasNext()) {
			Book book = it.next();
			sb.append(book.toString());
		}
		
		return sb.toString();
	}

}
