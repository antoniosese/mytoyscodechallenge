package com.mytoys.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mytoys.book.entity.Book;

public interface IBookRepository extends JpaRepository<Book, Long> {
	
}